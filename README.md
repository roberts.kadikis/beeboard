# BeeBoard

The code for the BeeBoard MIDI controller.

# Workings
...

# Dependencies / Installation

## Compilation dependencies

## Use compiled program

Download and install [LoopBe1](https://www.nerds.de/en/download.html).
The LoopBe1 must be running while the BeeBoard is used. Virtual instrumnents will see the LoopBe Internal MIDI device. For some instruments you might need to enable this MIDI device as a MIDI input, usually, in a menu called something like "Audio and MIDI Settings".

If BeeBoard has a PS3EYE camera inside, you will need to install a libusb driver. One way to do this is to download and run a driver manager [Zadig](http://zadig.akeo.ie/). When downloaded, plug in BeeBoard, run Zadig, find the camera (USB Camera-...), choose the driver (libusb-win32 (v...)), and install.

