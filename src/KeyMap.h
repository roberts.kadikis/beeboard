﻿
#ifndef __KEYMAP_H_INCLUDED__
#define __KEYMAP_H_INCLUDED__

#define _CRT_SECURE_NO_DEPRECATE
#define WINVER 0x0500

# include <windows.h>
#include <string>
#include "opencv2/opencv.hpp"
#include <unordered_map>

using namespace cv;
using namespace std;


class KeyMap {
	unordered_map<string, int> key_map;

public:
	KeyMap(int numberOfKeys) {
		namesToCode();			// create the hash table of names and corresponding MIDI codes
	}
	
	int getNoteCode(string noteName) { return key_map[noteName]; }

	void namesToCode() {
		key_map["C0"] =  0;
		key_map["C#0"] = 1;		key_map["Db0"] = 1;
		key_map["D0"] =  2;
		key_map["D#0"] = 3;		key_map["Eb0"] = 3;
		key_map["E0"] =  4;		key_map["Fb0"] = 4;
		key_map["F0"] =  5;		key_map["E#0"] = 5;
		key_map["F#0"] = 6;		key_map["Gb0"] = 6;
		key_map["G0"] =  7;		
		key_map["G#0"] = 8;		key_map["Ab0"] = 8;
		key_map["A0"] =  9;		
		key_map["A#0"] = 10;	key_map["Bb0"] = 10;
		key_map["B0"] =  11;	key_map["Cb1"] = 11;

		key_map["C1"] =  12;
		key_map["C#1"] = 13;	key_map["Db1"] = 13;
		key_map["D1"] =  14;
		key_map["D#1"] = 15;	key_map["Eb1"] = 14;
		key_map["E1"] =  16;	key_map["Fb1"] = 16;
		key_map["F1"] =  17;	key_map["E#1"] = 17;
		key_map["F#1"] = 18;	key_map["Gb1"] = 18;
		key_map["G1"] =  19;
		key_map["G#1"] = 20;	key_map["Ab1"] = 20;
		key_map["A1"] =  21;
		key_map["A#1"] = 22;	key_map["Bb1"] = 22;
		key_map["B1"] =  23;	key_map["Cb2"] = 23;

		key_map["C2"] =  24;
		key_map["C#2"] = 25;	key_map["Db2"] = 25;
		key_map["D2"] =  26;
		key_map["D#2"] = 27;	key_map["Eb2"] = 27;
		key_map["E2"] =  28;	key_map["Fb2"] = 28;
		key_map["F2"] =  29;	key_map["E#2"] = 29;
		key_map["F#2"] = 30;	key_map["Gb2"] = 30;
		key_map["G2"] =  31;
		key_map["G#2"] = 32;	key_map["Ab2"] = 32;
		key_map["A2"] =  33;
		key_map["A#2"] = 34;	key_map["Bb2"] = 34;
		key_map["B2"] =  35;	key_map["Cb3"] = 35;

		key_map["C3"] =  36;
		key_map["C#3"] = 37;	key_map["Db3"] = 37;
		key_map["D3"] =  38;
		key_map["D#3"] = 39;	key_map["Eb3"] = 39;
		key_map["E3"] =  40;	key_map["Fb3"] = 40;
		key_map["F3"] =  41;	key_map["E#3"] = 41;
		key_map["F#3"] = 42;	key_map["Gb3"] = 42;
		key_map["G3"] =  43;
		key_map["G#3"] = 44;	key_map["Ab3"] = 44;
		key_map["A3"] =  45;
		key_map["A#3"] = 46;	key_map["Bb3"] = 46;
		key_map["B3"] =  47;	key_map["Cb4"] = 47;

		key_map["C4"] =  48;
		key_map["C#4"] = 49;	key_map["Db4"] = 49;
		key_map["D4"] =  50;
		key_map["D#4"] = 51;	key_map["Eb4"] = 51;
		key_map["E4"] =  52;	key_map["Fb4"] = 52;
		key_map["F4"] =  53;	key_map["E#4"] = 53;
		key_map["F#4"] = 54;	key_map["Gb4"] = 54;
		key_map["G4"] =  55;
		key_map["G#4"] = 56;	key_map["Ab4"] = 56;
		key_map["A4"] =  57;
		key_map["A#4"] = 58;	key_map["Bb4"] = 58;
		key_map["B4"] =  59;	key_map["Cb5"] = 59;

		key_map["C5"] =  60;
		key_map["C#5"] = 61;	key_map["Db5"] = 61;
		key_map["D5"] =  62;
		key_map["D#5"] = 63;	key_map["Eb5"] = 63;
		key_map["E5"] =  64;	key_map["Fb5"] = 64;
		key_map["F5"] =  65;	key_map["E#5"] = 65;
		key_map["F#5"] = 66;	key_map["Gb5"] = 66;
		key_map["G5"] =  67;
		key_map["G#5"] = 68;	key_map["Ab5"] = 68;
		key_map["A5"] =  69;
		key_map["A#5"] = 70;	key_map["Bb5"] = 70;
		key_map["B5"] =  71;	key_map["Cb6"] = 71;

		key_map["C6"] =  72;
		key_map["C#6"] = 73;	key_map["Db6"] = 73;
		key_map["D6"] =  74;
		key_map["D#6"] = 75;	key_map["Eb6"] = 75;
		key_map["E6"] =  76;	key_map["Fb6"] = 76;
		key_map["F6"] =  77;	key_map["E#6"] = 77;
		key_map["F#6"] = 78;	key_map["Gb6"] = 78;
		key_map["G6"] =  79;
		key_map["G#6"] = 80;	key_map["Ab6"] = 80;
		key_map["A6"] =  81;
		key_map["A#6"] = 82;	key_map["Bb6"] = 82;
		key_map["B6"] =  83;	key_map["Cb7"] = 83;

		key_map["C7"] =  84;
		key_map["C#7"] = 85;	key_map["Db7"] = 85;
		key_map["D7"] =  86;
		key_map["D#7"] = 87;	key_map["Eb7"] = 87;
		key_map["E7"] =  88;	key_map["Fb7"] = 88;
		key_map["F7"] =  89;	key_map["E#7"] = 89;
		key_map["F#7"] = 90;	key_map["Gb7"] = 90;
		key_map["G7"] =  91;
		key_map["G#7"] = 92;	key_map["Ab7"] = 92;
		key_map["A7"] =  93;
		key_map["A#7"] = 94;	key_map["Bb7"] = 94;
		key_map["B7"] =  95;	key_map["Cb8"] = 95;

		key_map["C8"] =  96;
		key_map["C#8"] = 97;	key_map["Db8"] = 97;
		key_map["D8"] =  98;
		key_map["D#8"] = 99;	key_map["Eb8"] = 99;
		key_map["E8"] =  100;	key_map["Fb8"] = 100;
		key_map["F8"] =  101;	key_map["E#8"] = 101;
		key_map["F#8"] = 102;	key_map["Gb8"] = 102;
		key_map["G8"] =  103;
		key_map["G#8"] = 104;	key_map["Ab8"] = 104;
		key_map["A8"] =  105;
		key_map["A#8"] = 106;	key_map["Bb8"] = 106;
		key_map["B8"] =  107;	key_map["Cb9"] = 107;

		key_map["C9"] =  108;
		key_map["C#9"] = 109;	key_map["Db9"] = 109;
		key_map["D9"] =  110;
		key_map["D#9"] = 111;	key_map["Eb9"] = 111;
		key_map["E9"] =  112;	key_map["Fb9"] = 112;
		key_map["F9"] =  113;	key_map["E#9"] = 113;
		key_map["F#9"] = 114;	key_map["Gb9"] = 114;
		key_map["G9"] =  115;
		key_map["G#9"] = 116;	key_map["Ab9"] = 116;
		key_map["A9"] =  117;
		key_map["A#9"] = 118;	key_map["Bb9"] = 118;
		key_map["B9"] =  119;	key_map["Cb10"] = 119;

		key_map["C10"] =  120;
		key_map["C#10"] = 121;	key_map["Db10"] = 121;
		key_map["D10"] =  122;
		key_map["D#10"] = 123;	key_map["Eb10"] = 123;
		key_map["E10"] =  124;	key_map["Fb10"] = 124;
		key_map["F10"] =  125;	key_map["E#10"] = 125;
		key_map["F#10"] = 126;	key_map["Gb10"] = 126;
		key_map["G10"] =  127;
	}
};


#endif
