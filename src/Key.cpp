# include "Key.h"


// Constructor
Key::Key(Mat keyPatch, Point keyCoordinate, Rect keyROI, int keyNumber, string keyNote, int keyNoteCode) {
	patchReference = keyPatch;
	center = keyCoordinate;
	roi = keyROI;
	number = keyNumber;
	note = keyNote;
	noteCode = keyNoteCode;
	state = false;
	statePrevious = state;

	Mat bw;
	//n  threshold = cv::threshold(patchReference, bw, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);		// cout << "thr: " << threshold << endl;
	areaReference = sum(bw)[0] / 255;

	noteVelocity = 90; // ... temporary!!
}

// Determine the state of the key - is it on or off
void Key::detectState(float threshold) {

	statePrevious = state;

	Mat currentBw;
	cv::threshold(patch, currentBw, threshold, 255, CV_THRESH_BINARY);
	float totalArea = patch.rows * patch.cols;
	float currentArea = sum(currentBw)[0] / 255;
	float partArea = totalArea / 1.5;
	if (currentArea < partArea) {
		state = true;
		noteVelocity = ((partArea - currentArea) / partArea) * 127;
		if (noteVelocity < 1)
			noteVelocity = 1;
	}
	else
		state = false;
}


// Detect, if there is a state change of the key in current frame
bool Key::stateChange() {
	if (state == statePrevious)
		return false;
	else
		return true;
}


// Update the patch of current image
void Key::updatePatch(Mat inputImage) {
	patch = inputImage(roi);
}


void Key::velocityLog() {
	if (state && state != statePrevious) {
	//		cout << "vel before: " << noteVelocity << endl;
		noteVelocity = int(round( (log(noteVelocity + 20)-3)/2 * 127 ));
		//np.round((np.log(x + 20) - 3) / 2 * 127)


	//	cout << "vel after: " << noteVelocity << endl;
	}
}
