#ifndef __MAIN_H_INCLUDED__
#define __MAIN_H_INCLUDED__

#define _CRT_SECURE_NO_DEPRECATE
#define WINVER 0x0500


#include <iostream>
#include <fstream>
#include <sstream>
#include "opencv2/highgui.hpp"
#include "opencv2/opencv.hpp"
#include <windows.h>
#include "Key.h"
#include "KeyMap.h"
#include <cstdlib>
#include "RtMidi.h"
#include <time.h>
#include "libusb.h"
#include "ps3eye.h"
using namespace cv;
using namespace std;
using namespace ps3eye;


// Input parameters -------------------------------------------------
VideoCapture capture(CV_CAP_MIL);
int CAMERA_ID;
int FRAME_WIDTH; // 320; // 640;
int FRAME_HEIGHT; // 240; // 480;
int INPUT_FPS; // 60
int KEYS_ROWS;	
int KEYS_COLS; // for sorting the keys in the right order
double EXPOSURE; // -8
double MAX_EXPOSURE; // -7
double MIN_EXPOSURE; // -7
int HOLE_TO_WIDTH_RATIO; // 52
int NUMBER_OF_KEYS; // 140 or 280
int TRANSPOSITION;
float KEY_THRESHOLD;
int VISUALIZATION;
bool VELOCITY;
bool PS_CAMERA;
string NOTE_MAP_FILENAME;

ps3eye::PS3EYECam::PS3EYERef eye;
uint8_t *frame_bgr;

// -----------------------------------------------------------------


bool exposureOk = false;

// Keys and holes
int KEY_BATCH_SIZE;
RtMidiOut * midiout = new RtMidiOut();
std::vector<unsigned char> message;
vector <KeyPoint> keypoints;
vector <KeyPoint> keypointsSorted;
vector <int> assignedKeys;
vector <KeyPoint> keypointsSort;
vector <Key * > keys;
bool allHolesDetected;
vector <string> noteMap;


// Images
Mat inputFrame;
vector <Mat> rgb;
Mat greyFrame;
Mat bwFrame;
SimpleBlobDetector::Params params;


int firstFrame;
double thresholdFirst = 100;


// Functions
void loadConfig();
void loadNoteMap();
void openCamera();
void openMIDI();
void preProcessFrame();
void configureBlobDetector();
void sortKeys();
void visualizeKeypoints();
void createKeys(KeyMap * keyMap);
void processKeys();
void visualizeKeys();



#endif