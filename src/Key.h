#ifndef __KEY_H_INCLUDED__
#define __KEY_H_INCLUDED__

#define _CRT_SECURE_NO_DEPRECATE
#define WINVER 0x0500

# include <windows.h>
#include <string>
#include "opencv2/opencv.hpp"
#include <math.h>

using namespace cv;
using namespace std;

class Key {
	Mat patch;
	Mat patchReference;
	Mat patchRed;
	Mat patchGreen;
	Mat patchBlue;
	double areaReference;
	double previousRedness;
	Rect roi;
	Point center;
	int size;
	int number;
	bool state;
	bool statePrevious;
	string note;
	int noteCode;
	int noteVelocity;

//	double threshold;

public:
	Key(Mat keyPatch, Point keyCoordinate, Rect keyROI, 
		int keyNumber, string keyNote, int keyNoteCode);
	void detectState(float threshold);
	bool stateChange();
	void updatePatch(Mat inputImage);
	void velocityLog();

	Point getCenter() { return center; }
	string getNote() { return note; }
	int getNoteCode() { return noteCode;  }
	int getNumber() { return number; }
	Rect getRoi() { return roi; }
	bool getState() { return state;  }
	int  getNoteVelocity() { return noteVelocity; }

};


#endif