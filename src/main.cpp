# include "main.h"


int main(int argc, char **argv) {
	cout << "##########################" << endl;
	cout << "#######       BeeBoard" << endl << endl;

	loadConfig();

	loadNoteMap();

	bool allKeysFound = false;
	
	cout << endl;
	cout << "# Input parameters:" << endl;
	cout << "	Frame width = " << FRAME_WIDTH << endl;
	cout << "	Frame height = " << FRAME_HEIGHT << endl;
	cout << "	FPS = " << INPUT_FPS << endl;
	cout << "	Exposure = " << EXPOSURE << endl;
	cout << endl;


	cout << "# Detecting keys " << endl;

//	VideoWriter video("out.avi", CV_FOURCC('M', 'J', 'P', 'G'), 10, Size(FRAME_WIDTH, FRAME_HEIGHT), true);
	while (!allKeysFound) {

		/*
		cout << "counting cameras" << endl;
		cv::VideoCapture temp_camera;
		int maxTested = 10;
		for (int i = 0; i < maxTested; i++) {
			cv::VideoCapture temp_camera(i);
			bool res = (!temp_camera.isOpened());
			temp_camera.release();
			if (res)
			{
				cout << "ccam i   " << i << endl;
			}
		}
		*/

		openCamera();

		if (PS_CAMERA){
			frame_bgr = new uint8_t[eye->getWidth()*eye->getHeight() * 3];
			eye->getFrame(frame_bgr);
			inputFrame = Mat(eye->getHeight(), eye->getWidth(), CV_8UC3, frame_bgr);
		}
		else {
			capture.read(inputFrame);
		}

		imshow("Input", inputFrame);
		int key = waitKey(1);
		if (key == 27)
			break;

		preProcessFrame();	

		configureBlobDetector();

		Ptr<SimpleBlobDetector> d = SimpleBlobDetector::create(params);

		d->detect(greyFrame, keypoints);						
		cout << "\r  " << keypoints.size() <<  " of " << NUMBER_OF_KEYS << " keys detected" << flush;

		if (keypoints.size() == NUMBER_OF_KEYS) {

			allKeysFound = true;

			float sumKeySize = 0;
			for (int i = 0; i < keypoints.size(); i++) {
				sumKeySize = sumKeySize + keypoints[i].size;
			}
			float avgKeySize = sumKeySize / keypoints.size();

			for (int i = 0; i < keypoints.size(); i++) {

				if (keypoints[i].size > 2 * avgKeySize ||
					keypoints[i].size < 0.5 * avgKeySize) {
					cout << "avg: " << avgKeySize << " current: " << keypoints[i].size << endl;
					cout << "Too big or too small key found! Repeating the search!" << endl;
					allKeysFound = false;
					break;
				}
			}
		}

		if (PS_CAMERA) {
			delete [] frame_bgr;
		}
	}
	cout << endl;
	cout << endl;
	/*
	cout << "# Actual camera parameters:" << endl;
	cout << "	Frame width = " << capture.get(CV_CAP_PROP_FRAME_WIDTH) << endl;
	cout << "	Frame height = " << capture.get(CV_CAP_PROP_FRAME_HEIGHT) << endl;
	cout << "	FPS = " << capture.get(CV_CAP_PROP_FPS) << endl;
	cout << "	FPS2 = " << capture.get(CAP_PROP_FPS) << endl;
	cout << "	Exposure = " << capture.get(CV_CAP_PROP_EXPOSURE) << endl;
	cout << "	Capture format " << capture.get(CV_CAP_PROP_FOURCC) << endl;

	cout << endl;
	*/

	sortKeys();

	KeyMap * keyMap = new KeyMap(NUMBER_OF_KEYS);

	createKeys(keyMap);

	openMIDI();

	// FPS counter
	time_t start, end;
	int counter = 0;
	double sec;
	double fps;
	string fpsString;

	cvDestroyAllWindows();
	cout << "# Let's buzz!" << endl;
	while (1) {
		// FPS counter
		if (counter == 0){
			time(&start);
		}

		if (PS_CAMERA) {
			frame_bgr = new uint8_t[eye->getWidth()*eye->getHeight() * 3];
			eye->getFrame(frame_bgr);
			inputFrame = Mat(eye->getHeight(), eye->getWidth(), CV_8UC3, frame_bgr);
		}
		else {
			capture.read(inputFrame);
		}

		//	video.write(inputFrame);

		preProcessFrame();

		processKeys();								
	
		if (VISUALIZATION > 0) {
			visualizeKeypoints();
			visualizeKeys();
			resize(inputFrame, inputFrame, Size(640, 320), 0, 0, INTER_CUBIC);
			imshow("Input", inputFrame);

			int key = waitKey(1);
			if (key == 27)
				break;
		}



		time(&end);
		counter++;
		sec = difftime(end, start);
		fps = counter / sec;
		if (counter > 30)
			fpsString = to_string(fps);
			cout << "\r" << "FPS: " <<  fpsString.substr(0, 2) << flush;
		if (counter == (INT_MAX - 1000))
			counter = 0;
	
		if (PS_CAMERA) {
			delete[] frame_bgr;
		}
	}
	destroyAllWindows();
//	capture.set(CV_CAP_PROP_EXPOSURE, 0);
	capture.release();
	delete midiout;

	cout << endl;
	cout << "########     GP2 has ended" << endl;
	cout << "##########################" << endl;
}


void loadConfig() {
	ifstream read;
	string configFileName = "config.txt";
	read.open(configFileName);

	if (read.is_open())
		cout << "# Reading input pratameters from " << configFileName << endl;
	else
		cout << "Error opening " << configFileName << ".\n";
	string line;
	while (getline(read, line)) {
		istringstream sin(line.substr(line.find("=") + 1));

		string tempProperty;
		if (line.find("FRAME_WIDTH") != -1) {
			sin >> tempProperty;
			FRAME_WIDTH = atoi(tempProperty.c_str());
		}
		if (line.find("FRAME_HEIGHT") != -1) {
			sin >> tempProperty;
			FRAME_HEIGHT = atoi(tempProperty.c_str());
		}
		if (line.find("INPUT_FPS") != -1) {
			sin >> tempProperty;
			INPUT_FPS = atoi(tempProperty.c_str());
		}
		if (line.find("KEYS_ROWS") != -1) {
			sin >> tempProperty;
			KEYS_ROWS = atoi(tempProperty.c_str());
		}
		if (line.find("KEYS_COLS") != -1) {
			sin >> tempProperty;
			KEYS_COLS = atoi(tempProperty.c_str());
		}
		if (line.find("EXPOSURE") != -1) {
			sin >> tempProperty;
			EXPOSURE = atoi(tempProperty.c_str());
		}
		if (line.find("MAX_EXPOSURE") != -1) {
			sin >> tempProperty;
			MAX_EXPOSURE = atoi(tempProperty.c_str());
		}
		if (line.find("MIN_EXPOSURE") != -1) {
			sin >> tempProperty;
			MIN_EXPOSURE = atoi(tempProperty.c_str());
		}
		if (line.find("HOLE_TO_WIDTH_RATIO") != -1) {
			sin >> tempProperty;
			HOLE_TO_WIDTH_RATIO = atoi(tempProperty.c_str());
		}
		if (line.find("NUMBER_OF_KEYS") != -1) {
			sin >> tempProperty;
			NUMBER_OF_KEYS = atoi(tempProperty.c_str());
		}
		if (line.find("KEY_THRESHOLD") != -1) {
			sin >> tempProperty;
			KEY_THRESHOLD = atoi(tempProperty.c_str());
		}
		if (line.find("TRANSPOSITION") != -1) {
			sin >> tempProperty;
			TRANSPOSITION = atoi(tempProperty.c_str());
		}
		if (line.find("CAMERA_ID") != -1) {
			sin >> tempProperty;
			CAMERA_ID = atoi(tempProperty.c_str());
		}
		if (line.find("PS_CAMERA") != -1) {
			sin >> tempProperty;
			PS_CAMERA = atoi(tempProperty.c_str());
		}
		if (line.find("VISUALIZATION") != -1) {
			sin >> tempProperty;
			VISUALIZATION = atoi(tempProperty.c_str());
		}
		if (line.find("NOTE_MAP_FILENAME") != -1) {
			sin >> NOTE_MAP_FILENAME;
		}
		if (line.find("VELOCITY") != -1) {
			sin >> tempProperty;
			VELOCITY = atoi(tempProperty.c_str());
		}
	}
}


void loadNoteMap() {
	ifstream read;
	string noteMapFileName = NOTE_MAP_FILENAME + ".txt";
	read.open(noteMapFileName);

	if (read.is_open())
		cout << "# Using notemap " << NOTE_MAP_FILENAME << endl;
	else
		cout << "Error opening " << NOTE_MAP_FILENAME << ".\n";
	
	string line;
	int currentOctave = TRANSPOSITION;
	int previousNoteIdx = -1;
	int noteCount = -1;
	int firstNoteIdx;
	bool firstNoteIdxDetected = false;
	int previousFirstNoteIdx;
	while (getline(read, line)) {
		if (!line.empty()) {
			istringstream sin(line.substr(line.find("=") + 1));
			if (TRANSPOSITION == -1)
				noteMap.push_back(line);
			else {

				int noteIdx;
				if (line[0] == 'C')
					noteIdx = 0;
				if (line[0] == 'D')
					noteIdx = 1;
				if (line[0] == 'E')
					noteIdx = 2;
				if (line[0] == 'F')
					noteIdx = 3;
				if (line[0] == 'G')
					noteIdx = 4;
				if (line[0] == 'A')
					noteIdx = 5;
				if (line[0] == 'B')
					noteIdx = 6;

				
				if (!firstNoteIdxDetected) {
					firstNoteIdx = noteIdx;
					firstNoteIdxDetected = true;
					previousFirstNoteIdx = firstNoteIdx;
				}

				// automatic transposition jump among key columns
				noteCount++;
				if (noteCount % KEYS_COLS == 0) {
					currentOctave = TRANSPOSITION;
					if (noteIdx > previousFirstNoteIdx) {
						if (noteIdx - previousFirstNoteIdx >= previousFirstNoteIdx + 6 - noteIdx)
							currentOctave--;		
					}
					if (noteIdx < previousFirstNoteIdx) {
						if (previousFirstNoteIdx - noteIdx >= noteIdx + 6 - previousFirstNoteIdx)
							currentOctave++;
					}
				}
	
				// clip positions over usable MIDI values
				else if (noteIdx < previousNoteIdx) {
					currentOctave += 1;
					if (currentOctave > 10)
						currentOctave = 10;
					if (currentOctave < 0)
						currentOctave = 0;
				}
				previousNoteIdx = noteIdx;

				string transposition = to_string(currentOctave);
				string fullNoteName = line + transposition;
				noteMap.push_back(fullNoteName);
			}
		}
	}
}

void openCamera() {
		
	//	capture.VI.listDevices();
	if (PS_CAMERA) {
		std::vector<PS3EYECam::PS3EYERef> devices(PS3EYECam::getDevices());

		if (devices.size()) {
			eye = devices.at(0);
			bool res = eye->init(FRAME_WIDTH, FRAME_HEIGHT, INPUT_FPS);
			eye->setExposure(EXPOSURE);
			eye->start();
		}
		else {
			cout << "Error: didn't find any ps3eye cameras" << endl;
		}

		}
	else {

		int fourcc = CV_FOURCC('H', '2', '6', '4');
		capture.open(CAMERA_ID);
		if (!capture.isOpened()) {
			fprintf(stderr, "Could not initialize capturing...\n");
		}


		//		capture.set(CV_CAP_PROP_FOURCC, CV_FOURCC('M', 'J', 'P', 'G'));
		//		capture.set(CV_CAP_PROP_FOURCC, CV_FOURCC('Y', 'U', 'Y', '2'));
		capture.set(CV_CAP_PROP_FOURCC, CV_FOURCC('M', 'J', 'P', 'G'));
		//	capture.set(CV_CAP_PROP_FOURCC, CV_FOURCC('H', '2', '6', '4'));

		//	capture.set(CV_CAP_PROP_FOURCC, 1196444237);
		capture.set(CV_CAP_PROP_FPS, INPUT_FPS);
		capture.set(CV_CAP_PROP_FRAME_WIDTH, FRAME_WIDTH);
		capture.set(CV_CAP_PROP_FRAME_HEIGHT, FRAME_HEIGHT);
		//		capture.set(CV_CAP_PROP_EXPOSURE, EXPOSURE);

		cout << "	Capture format " << capture.get(CV_CAP_PROP_FOURCC) << endl;
	}

}


void openMIDI() {
	// Check available ports.
	unsigned int nPorts = midiout->getPortCount();
	if (nPorts == 0) {
		std::cout << "No MIDI ports available!\n";
		delete midiout;
	}
	// Find and open LoopBe port.
	for (int i = 0; i < nPorts; i++) {
		string portName = midiout->getPortName(i);
		if (portName.compare(0, 6, "LoopBe") == 0) {
			midiout->openPort(i);
			break;
		}
		//cout << "port open: " << portOpen << "	port name: " << portName << endl;
		if (i == nPorts - 1)
			cout << "LoopBe port not found! Is it installed and rynning? You can download free LoopBe1 at http://www.nerds.de/en/download.html" << endl;
	}
	// Send out a series of MIDI messages.
	// Program change: 192, 5
	message.push_back(192);
	message.push_back(5);
	midiout->sendMessage(&message);
	// Control Change: 176, 7, 100 (volume)
	message[0] = 176;
	message[1] = 7;
	message.push_back(100);
	midiout->sendMessage(&message);
}

void preProcessFrame() {
	vector<Mat> rgb;
	split(inputFrame, rgb);
	flip(rgb[0], rgb[0], 1);
	flip(rgb[1], rgb[1], 1);
	flip(rgb[2], rgb[2], 1);
	merge(rgb, inputFrame);
	cvtColor(inputFrame, greyFrame, CV_RGB2GRAY);
	//	threshold(greyFrame, bwFrame, thresholdFirst, 255, 0);
}


void configureBlobDetector() {
	params.minThreshold = 100.0f;
	params.maxThreshold = 200.0f;
	params.filterByArea = true;
	int minHoleArea = (FRAME_WIDTH / HOLE_TO_WIDTH_RATIO) ^ 2;					//	cout << "BLOB params: min hole area : " << minHoleArea << endl;
	params.minArea = minHoleArea;
	params.filterByCircularity = true;
	params.minCircularity = 0.3;
	params.filterByConvexity = true;
	params.minConvexity = 0.3;
	params.filterByInertia = true;
	params.minInertiaRatio = 0.3;
	params.filterByColor = false;
}


void visualizeKeypoints() {
	if (VISUALIZATION > 1) {
		drawKeypoints(inputFrame, keypoints, inputFrame, Scalar(0, 0, 255), DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
		drawKeypoints(inputFrame, keypointsSorted, inputFrame, Scalar(0, 255, 0), DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
	}

	if (VISUALIZATION == 3) {
		for (int i = 0; i < keypointsSorted.size(); i++) {
			string iString = to_string(i + 1);
			putText(inputFrame, iString, Point(keypointsSorted[i].pt.x + 7, keypointsSorted[i].pt.y + 15), FONT_HERSHEY_DUPLEX, 0.4, Scalar(40, 34, 200), 1.4, 8, false);
		}
	}
	resize(inputFrame, inputFrame, Size(FRAME_WIDTH, FRAME_HEIGHT), 0, 0, INTER_CUBIC);
}

void sortKeys() {

	// find the most bottom left corner key
	int minCornerScore = 1000;
	int minCornerId = 1;
	for (int i = 0; i < keypoints.size(); i++) {
		int height = greyFrame.rows;
		int width = greyFrame.cols;
		int cornerScore = (height - keypoints[i].pt.y) + keypoints[i].pt.x;
		if (cornerScore < minCornerScore) {
			minCornerScore = cornerScore;
			minCornerId = i;
		}
	}
	keypointsSorted.push_back(keypoints[minCornerId]);
	assignedKeys.push_back(minCornerId);

	// numerate the rest keys after the nr 1 is found
	int currentKey = 0;
	for (int i = 1; i < keypoints.size(); i++) {
		int bestDistValue = 0;
		int bestKeyId = 0;
		for (int j = 0; j < keypoints.size(); j++) {
			bool keyAssigned = false;
			for (int k = 0; k < assignedKeys.size(); k++) {
				if (j == assignedKeys[k])
					keyAssigned = true;
			}
			if (keyAssigned == false) {
				int xDist = keypoints[j].pt.x - keypointsSorted[i-1].pt.x;
				if (xDist > 10 || i % (KEYS_COLS) == 0) {
					int distValue = 1.5*keypoints[j].pt.y - xDist; // constant '1.5' might be changed
					if (distValue > bestDistValue) {
						bestDistValue = distValue;
						bestKeyId = j;
					}
				}
			}
		}
		keypointsSorted.push_back(keypoints[bestKeyId]);
		assignedKeys.push_back(bestKeyId);
	}
}



void createKeys(KeyMap * keyMap) {
	for (int i = 0; i < NUMBER_OF_KEYS; i++) {
		// create a key (cut the appropriate patch from the input)
		int keySize = FRAME_WIDTH / HOLE_TO_WIDTH_RATIO;
		Point keyCoordinate = keypointsSorted[i].pt;

		int keyW = keypointsSorted[i].size-2;
		int keyH = keypointsSorted[i].size-2;
		int keyX = keyCoordinate.x - keyW/2;
		int keyY = keyCoordinate.y - keyH/2;


		while (keyX < 1) { keyX++; }
		while (keyY < 1) { keyY++; }
		while (keyX + keyW > FRAME_WIDTH-2) { keyW--; }
		while (keyY + keyH > FRAME_HEIGHT-2) { keyH--; }

		Rect keyROI(keyX, keyY, keyW, keyH);
		Mat keyPatch = greyFrame(keyROI);
		int keyNumber = i;
		string keyNote = noteMap[i];
		int keyNoteCode = keyMap->getNoteCode(keyNote);
		Key * key = new Key(keyPatch, keyCoordinate, keyROI, keyNumber, keyNote, keyNoteCode);

		// add the key to the key vector
		keys.push_back(key);
	}
}


void processKeys() {
	for (int i = 0; i < NUMBER_OF_KEYS; i++) {
		Key * key = keys[i];
		key->updatePatch(greyFrame);
		key->detectState(KEY_THRESHOLD);
		key->velocityLog();

		if (key->stateChange()) {
			if (key->getState())
				message[0] = 144;
			else
				message[0] = 128;
			message[1] = key->getNoteCode();					//	cout << "note code: " << key->getNoteCode() << endl;
			if (VELOCITY)
				message[2] = key->getNoteVelocity();				//	cout << "note velocity: " << key->getNoteVelocity() << endl;
			else
				message[2] = 127;
			midiout->sendMessage(&message);
			//Sleep(1);											//	cout << " message sent! " << endl;
		}
	}
}


void visualizeKeys() {
	for (int i = 0; i < NUMBER_OF_KEYS; i++) {
		Key * key = keys[i];

		if (VISUALIZATION > 1) {
			if (key->getState())
				rectangle(inputFrame, key->getRoi(), Scalar(0, 255, 0), 1, 8, 0);
			else
				rectangle(inputFrame, key->getRoi(), Scalar(255, 0, 0), 1, 8, 0);
		}

		if (VISUALIZATION == 4) {
			char keyNumber[10];
			sprintf_s(keyNumber, "%d", key->getNumber());
			//	putText(inputFrame, keyNumber, key->getCenter(), FONT_HERSHEY_SIMPLEX, 0.5, CV_RGB(255, 0, 0), 1, 8, false);
			putText(inputFrame, key->getNote(), Point(key->getCenter().x + 8, key->getCenter().y + 2), FONT_HERSHEY_SIMPLEX, 0.4, CV_RGB(170, 0, 150), 1, 8, false);
		}
	}
}
